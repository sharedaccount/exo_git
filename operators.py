import bpy
import tempfile
from pathlib import Path
import shutil
import subprocess

from bpy.types import (
    Operator, PropertyGroup, UIList, WindowManager,
    Scene, Menu,
)
from bpy.props import (
    StringProperty, IntProperty, BoolProperty,
    PointerProperty, CollectionProperty,
    FloatProperty, FloatVectorProperty,
    EnumProperty,
)
from bl_operators.presets import AddPresetBase
from bpy.utils import is_path_builtin

import os
from os.path import basename, splitext

class BRT_OT_playblast(Operator):
    '''Render animation playblast'''
    bl_idname = "brt.playblast"
    bl_label = "Quick Playblast"
    bl_description = "Render a quick animation playblast"
    bl_options = {'UNDO'}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        '''main method running the command'''
        with contextual():
            CP = CustomPreview()    
            CP.set_preview_params()
            CP._render()
            CP._play()

        return {'FINISHED'}

class BRT_ExecutePreset(Operator):
    """Execute a preset"""
    bl_idname = "brt.execute_preset"
    bl_label = "Execute a Python Preset"

    filepath: StringProperty(
        subtype='FILE_PATH',
        options={'SKIP_SAVE'},
    )
    menu_idname: StringProperty(
        name="Menu ID Name",
        description="ID name of the menu this was called from",
        options={'SKIP_SAVE'},
    )

    def execute(self, context):
        
        filepath = self.filepath
        preset_class = getattr(bpy.types, self.menu_idname)
        preset_class.bl_label = bpy.path.display_name(basename(filepath))
        ext = splitext(filepath)[1].lower()
        if ext not in {".py", ".xml"}:
            self.report({'ERROR'}, "unknown filetype: %r" % ext)
            return {'CANCELLED'}

        if hasattr(preset_class, "reset_cb"):
            preset_class.reset_cb(context)

        if ext == ".py":
            try:
                bpy.utils.execfile(filepath)
            except Exception as ex:
                self.report({'ERROR'}, "Failed to execute the preset: " + repr(ex))

        elif ext == ".xml":
            import rna_xml
            rna_xml.xml_file_run(context,
                                 filepath,
                                 preset_class.preset_xml_map)

        if hasattr(preset_class, "post_cb"):
            preset_class.post_cb(context)

        return {'FINISHED'}

class BRT_MT_presetsmenu(Menu):
    bl_label = "Render Presets"
    bl_idname = "BRT_MT_presetsmenu"
    preset_subdir = "brt_presets/render"
    preset_operator = "brt.execute_preset"
    preset_operator_defaults = {
            "menu_idname" : 'BRT_MT_presetsmenu'
            }
    draw = Menu.draw_preset
    @classmethod
    def reset_cb(self, context):
        print("Reset")
    @classmethod        
    def post_cb(self, context):

        print("Post Callback")


class BRTAddPresetBase:
    preset_subdir = "brt_presets/render"
    bl_options = {'REGISTER', 'INTERNAL'}

    name: StringProperty(
        name="Name",
        description="Name of the preset, used to make the path name",
        maxlen=64,
        options={'SKIP_SAVE'},
    )
    remove_name: BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'},
    )
    remove_active: BoolProperty(
        default=False,
        options={'HIDDEN', 'SKIP_SAVE'},
    )

    @staticmethod
    def as_filename(name):

        def maketrans_init():
            cls = BRTAddPresetBase
            attr = "_as_filename_trans"

            trans = getattr(cls, attr, None)
            if trans is None:
                trans = str.maketrans({char: "_" for char in " !@#$%^&*(){}:\";'[]<>,.\\/?"})
                setattr(cls, attr, trans)
            return trans

        name = name.lower().strip()
        name = bpy.path.display_name_to_filepath(name)
        trans = maketrans_init()
        return name.translate(trans)

    def execute(self, context):

        if hasattr(self, "pre_cb"):
            self.pre_cb(context)

        preset_menu_class = getattr(bpy.types, self.preset_menu)

        is_xml = getattr(preset_menu_class, "preset_type", None) == 'XML'

        if is_xml:
            ext = ".xml"
        else:
            ext = ".py"

        name = self.name.strip()
        if not (self.remove_name or self.remove_active):

            if not name:
                return {'FINISHED'}

            wm = bpy.data.window_managers[0]
            if name == wm.preset_name:
                wm.preset_name = 'New Preset'

            filename = self.as_filename(name)
            target_path = os.path.join("presets", self.preset_subdir)
            script_path = bpy.utils.user_resource('SCRIPTS')
            target_path = os.path.join(script_path, target_path)
            print(target_path)
            if not target_path:
                self.report({'WARNING'}, "Failed to create presets path")
                return {'CANCELLED'}

            filepath = os.path.join(target_path, filename) + ext

            if hasattr(self, "add"):
                self.add(context, filepath)
            else:
                if is_xml:
                    import rna_xml
                    rna_xml.xml_file_write(context,
                                           filepath,
                                           preset_menu_class.preset_xml_map)
                else:

                    def rna_recursive_attr_expand(value, rna_path_step, level):
                        if isinstance(value, bpy.types.PropertyGroup):
                            for sub_value_attr in value.bl_rna.properties.keys():
                                if sub_value_attr == "rna_type":
                                    continue
                                sub_value = getattr(value, sub_value_attr)
                                rna_recursive_attr_expand(sub_value, "%s.%s" % (rna_path_step, sub_value_attr), level)
                        elif type(value).__name__ == "bpy_prop_collection_idprop":
                            file_preset.write("%s.clear()\n" % rna_path_step)
                            for sub_value in value:
                                file_preset.write("item_sub_%d = %s.add()\n" % (level, rna_path_step))
                                rna_recursive_attr_expand(sub_value, "item_sub_%d" % level, level + 1)
                        else:
                            #not advised but safe here
                            try:
                                value = value[:]
                            except:
                                pass

                            file_preset.write("%s = %r\n" % (rna_path_step, value))

                    file_preset = open(filepath, 'w', encoding="utf-8")
                    file_preset.write("import bpy\n")

                    if hasattr(self, "preset_defines"):
                        for rna_path in self.preset_defines:
                            exec(rna_path)
                            file_preset.write("%s\n" % rna_path)
                        file_preset.write("\n")

                    for rna_path in self.preset_values:
                        value = eval(rna_path)
                        rna_recursive_attr_expand(value, rna_path, 1)

                    file_preset.close()

            preset_menu_class.bl_label = bpy.path.display_name(filename)

        else:
            if self.remove_active:
                name = preset_menu_class.bl_label

            filepath = bpy.utils.preset_find(name,
                                             self.preset_subdir,
                                             ext=ext)

            if not filepath:
                filepath = bpy.utils.preset_find(name,
                                                 self.preset_subdir,
                                                 display_name=True,
                                                 ext=ext)

            if not filepath:
                return {'CANCELLED'}

            if is_path_builtin(filepath):
                self.report({'WARNING'}, "You can't remove the default presets")
                return {'CANCELLED'}

            try:
                if hasattr(self, "remove"):
                    self.remove(context, filepath)
                else:
                    os.remove(filepath)
            except Exception as e:
                self.report({'ERROR'}, "Unable to remove preset: %r" % e)
                import traceback
                traceback.print_exc()
                return {'CANCELLED'}

            preset_menu_class.bl_label = "Presets"

        if hasattr(self, "post_cb"):
            self.post_cb(context)

        return {'FINISHED'}


class BRT_OT_addpreset(BRTAddPresetBase, Operator):
    '''Add a Object Display Preset'''
    bl_idname = "brt.addpreset"
    bl_label = "Add Object Display Preset"
    preset_menu = "BRT_MT_presetsmenu"

    preset_defines = [
        "brt_props = bpy.context.scene.brt_props",          
    ]

    preset_values = [
    "brt_props.sound_on",    
    "brt_props.simplify_on",
    "brt_props.only_selected",
    "brt_props.start_frame",
    "brt_props.end_frame",
    "brt_props.render_percent",
    "brt_props.renderers",
    "brt_props.enable_copy",
    "brt_props.copy_dir",
    "brt_props.cycles_samples_render",
    "brt_props.eevee_samples_render", 
    "brt_props.global_ao", 
    "brt_props.use_stamps" 
    ]
    preset_subdir = "brt_presets/render"


def panel_func(self, context):
    layout = self.layout
    row = layout.row(align=True)
    row.menu(BRT_MT_presetsmenu.__name__, text=BRT_MT_presetsmenu.bl_label)


class contextual():
    '''temporary render settings restored after script execution'''
    def __init__(self):
        '''stores render settings '''
        sn = bpy.context.scene
        brt_props = sn.brt_props
        bpy.context.view_layer.update()
        self.settings = [a_prop.identifier for a_prop in bpy.context.scene.render.bl_rna.properties if not a_prop.is_readonly ]                      
        self.render_dict = {sett: getattr(bpy.context.scene.render, sett) for sett in self.settings}
        self.selection = bpy.context.selected_objects
        #keep only audio track in the sequencer
        sq = sn.sequence_editor
        self.hidden_obj = []
        self.video_tracks = []
        self.initial_samples = 32
        self.use_ao = False
        self.framerange = [bpy.context.scene.frame_start, bpy.context.scene.frame_end]
        for seq in sq.sequences:
            seq.select = False
            if seq.type != 'SOUND' or not brt_props.sound_on :
                self.video_tracks.append((seq.filepath, seq.name, seq.channel, seq.frame_start),)
                seq.select = True
                bpy.ops.sequencer.delete()
        if brt_props.only_selected :
            for obj in bpy.data.objects:
                if not obj in self.selection and not obj.hide_render :
                    self.hidden_obj.append(obj)
                    obj.hide_render = True
        bpy.context.scene.render.engine = brt_props.renderers
        if bpy.context.scene.render.engine == "CYCLES" :
            self.initial_samples = bpy.context.scene.cycles.samples
            bpy.context.scene.cycles.samples = brt_props.cycles_samples_render
        if bpy.context.scene.render.engine == "BLENDER_EEVEE" :
            self.initial_samples = bpy.context.scene.eevee.taa_render_samples
            bpy.context.scene.eevee.taa_render_samples = brt_props.eevee_samples_render       
            self.use_ao = bpy.context.scene.eevee.use_gtao
        bpy.context.scene.frame_start = brt_props.start_frame
        bpy.context.scene.frame_end = brt_props.end_frame
    def __enter__(self):
        
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        brt_props = bpy.context.scene.brt_props
        for scene in bpy.data.scenes:
            for sett, val in self.render_dict.items():
                setattr(scene.render, sett, val)
        #restoring video tracks
        for seq in self.video_tracks:
            bpy.context.scene.sequence_editor.sequences.new_movie(filepath=seq[0], name=seq[1], channel=seq[2], frame_start=seq[3])
        #restoring outliner render visibility
        if brt_props.only_selected :
            for obj in self.hidden_obj:
                obj.hide_render = False
            self.hidden_obj = []
        #restoring selection       
        for obj in self.selection :
            obj.select_set(True)
        if bpy.context.scene.render.engine == "CYCLES" :
            bpy.context.scene.cycles.samples = self.initial_samples
        if bpy.context.scene.render.engine == "BLENDER_EEVEE" :
            bpy.context.scene.eevee.taa_render_samples = self.initial_samples
            bpy.context.scene.eevee.use_gtao = self.use_ao
        bpy.context.scene.frame_start = self.framerange[0]
        bpy.context.scene.frame_end = self.framerange[1]
            

class CustomPreview():
    '''Helper Class to generate quick playblasts'''    
    
    def stamp_set(self,scene,note=" "):
        '''Callback for customs notes in Stamp'''  
        note = str(bpy.context.scene.frame_current)
        scene.render.stamp_note_text = note

    def locate_script(self,_script=""):
        '''returns the path of a sript'''
        for _path in bpy.utils.script_paths() :
            script_path = Path(_path + _script)
            if script_path.exists():
                return str(script_path)
        return None        
           
    def set_preview_params(self):
        '''Configure Render settings for quick preview generation'''
        rd = bpy.context.scene.render
        brt_props = bpy.context.scene.brt_props
        rd.image_settings.file_format = 'FFMPEG'
        preset = self.locate_script("/presets/ffmpeg/H264_in_MP4.py")
        if brt_props.simplify_on :
            rd.use_simplify = True
            rd.simplify_subdivision = 0
            rd.simplify_subdivision_render = 0
        if preset is not None :
            bpy.ops.script.python_file_run(filepath=preset)
        rd.ffmpeg.format = 'QUICKTIME'
        rd.ffmpeg.gopsize = 5
        rd.ffmpeg.audio_codec = 'AC3'
        rd.use_sequencer = True
        rd.fps = 25
        for _props in [a_prop.identifier for a_prop in bpy.context.scene.render.bl_rna.properties if "use_stamp" in a_prop.identifier ]:
            setattr(bpy.data.scenes[0].render, _props, False)
        if brt_props.use_stamps :
            rd.use_stamp = True
            rd.use_stamp_note = True
            rd.stamp_font_size = 32
            bpy.app.handlers.render_pre.append(self.stamp_set)
        #setattr(bpy.data.scenes[0].render, "resolution_percentage", 50)
        rd.resolution_percentage = brt_props.render_percent
        preview_file = tempfile.gettempdir() + "/preview.mov"
        rd.filepath = preview_file
        self.output_file = preview_file
        if brt_props.global_ao :
            if brt_props.renderers == "BLENDER_EEVEE" :
                bpy.context.scene.eevee.use_gtao = True
                       
        bpy.context.view_layer.update()

    def _render(self):
        '''Renders animation'''
        _layer = bpy.context.view_layer
        _scene = bpy.context.scene
        active_cam = _scene.camera
        bpy.ops.render.render(animation=True, write_still=False, use_viewport=False, layer=_layer.name, scene=_scene.name)
        if _scene.brt_props.enable_copy and self.output_file:
            shutil.copyfile(self.output_file, _scene.brt_props.copy_dir+str(Path(self.output_file).name))
            
    
    def _play(self):
        '''Plays rendered animation'''
        bpy.ops.render.play_rendered_anim()
                              
