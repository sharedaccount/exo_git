import bpy
import os

from bpy.types import (
    Panel, Menu, AddonPreferences, PropertyGroup,
    UIList, WindowManager, Scene
)

from bl_ui.utils import PresetPanel

class BRT_AbstractPanel():
    '''abstract class defining the position of the children panel(s) '''
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "render"
    @classmethod
    def poll(cls, context):
        return True

class BRT_PT_tab(PresetPanel, Panel):
    bl_label = 'Presets'
    preset_subdir = "brt_presets/render"
    preset_operator = 'brt.execute_preset'
    preset_add_operator = 'brt.addpreset'


class BRT_PT_panel(BRT_AbstractPanel, Panel):
    '''Panel inheriting defaults from parents class used to display the panel'''
    bl_idname = "BRT_PT_panel"
    bl_label = "Blender Render tool"

    def draw_header_preset(self, _context):
        layout = self.layout
        BRT_PT_tab.draw_panel_header(self.layout)
        
    def draw(self, context):
        layout = self.layout
        row = layout.row(align=True)

class BRT_PT_buttons(BRT_AbstractPanel, Panel):
    '''button displayed in BRT_PT_panel'''
    bl_idname = "BRT_PT_buttons"
    bl_label = "Options"
    bl_parent_id = "BRT_PT_panel"
    bl_options = {'HIDE_HEADER'}
    bl_description = "Main panel of Blender Render tool"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        col = row.column(align = True)
        
        scene = bpy.context.scene
        brt_props = scene.brt_props

        col.operator('brt.playblast')
        col.separator()

        row = layout.row(align = True)
        row.use_property_decorate = False
        row.prop(brt_props, "start_frame" )
        row.prop(brt_props, "end_frame" )
        row = layout.row(align = True)
        row.prop(brt_props, "render_percent" )
        row = layout.row(align = True)
        row.alignment = 'LEFT'
        row.prop(brt_props, "renderers" )


class BRT_PT_options(BRT_AbstractPanel, Panel):

    bl_idname = "BRT_PT_options"
    bl_label = "Options"
    bl_parent_id = "BRT_PT_panel"
    bl_options = {'DEFAULT_CLOSED',}

    def draw(self, context):
        layout = self.layout
   
        
class BRT_PT_global_options(BRT_AbstractPanel, Panel):

    bl_idname = "BRT_PT_global_options"
    bl_label = "Options"
    bl_parent_id = "BRT_PT_options"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        scene = bpy.context.scene
        brt_props = scene.brt_props
        row = layout.row(align = True)
        row.prop(brt_props, "sound_on" )
        row = layout.row(align = True)
        row.prop(brt_props, "simplify_on" )
        
        row = layout.row(align = True)
        row.prop(brt_props, "only_selected" )
        if brt_props.renderers == "BLENDER_EEVEE" :
            row = layout.row(align = True)
            row.prop(brt_props, "global_ao", text = "Ambient Occlusion")
        row = layout.row(align = True)
        row.prop(brt_props, "use_stamps", text = "Burn Stamps")
        
        row = layout.row(align = True)
        row = row.split(factor=0.35 )
        row.alignment = 'LEFT'
        row.prop(brt_props, "enable_copy", text = "Copy to dir:")
        row.use_property_split = True
        row.use_property_decorate = False
        row = row.split(factor=0.99 )
        row.active = brt_props.enable_copy
        row.enabled = brt_props.enable_copy
        row.alignment = 'LEFT'
        row.prop(brt_props, "copy_dir", text = "")

                     
class BRT_PT_cycles_options(BRT_AbstractPanel, Panel):

    bl_idname = "BRT_PT_cycles_options"
    bl_label = "Cycles Options"
    bl_parent_id = "BRT_PT_options"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        scene = bpy.context.scene
        brt_props = scene.brt_props
        if brt_props.renderers == 'CYCLES' : 
            row.prop(brt_props, "cycles_samples_render")
     
                   
class BRT_PT_eevee_options(BRT_AbstractPanel, Panel):

    bl_idname = "BRT_PT_eevee_options"
    bl_label = "Eevee Options"
    bl_parent_id = "BRT_PT_options"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        scene = bpy.context.scene
        brt_props = scene.brt_props
        if brt_props.renderers == 'BLENDER_EEVEE' : 
            row.prop(brt_props, "eevee_samples_render")
      
               
def brt_menu_top(self, context):
    self.layout.menu('BRT_MT_menu_top')

class BRT_MT_menu_top(bpy.types.Menu):
    '''display button in top menu bar''' 
    bl_label = "Render tool"
    bl_idname = "BRT_MT_menu_top"
    bl_description = "Render animation playblast"

    def draw(self, context):
        layout = self.layout
        layout.operator('brt.playblast')

     
